package net.chenqiong.fastdfs.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;

@SpringBootTest
public class SpringbootApplicationTest {
	@Autowired
	private FastFileStorageClient storageClient;

	/**
	 * 文件上传
	 */
	@Test
	public void uploadTest() {
		InputStream is = null;
		try {
			// 获取文件源
			File source = new File("D:\\qrCode\\devOps.png");
			// 获取文件流
			is = new FileInputStream(source);
			// 进行文件上传
			StorePath storePath = storageClient.uploadFile(is, source.length(), FilenameUtils.getExtension(source.getName()), null);
			// 获得文件上传后访问地址
			String fullPath = storePath.getFullPath();
			// 打印访问地址
			System.out.println("fullPath = " + fullPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					// 关闭流资源
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
