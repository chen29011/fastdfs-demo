package net.chenqiong.fastdfs.utils;

import javax.annotation.Resource;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;

@Component
public class FileDfsUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileDfsUtil.class);
	@Resource
	private FastFileStorageClient storageClient;

	/**
	 * 上传文件
	 */
	public String upload(MultipartFile file) throws Exception {
		StorePath storePath = storageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
		// 获得文件上传后访问地址
		String fullPath = storePath.getFullPath();
		// 打印访问地址
		System.out.println("fullPath = " + fullPath);
		return storePath.getFullPath();
	}

	/**
	 * 删除文件
	 */
	public void deleteFile(String fileUrl) {
		if (StringUtils.isEmpty(fileUrl)) {
			LOGGER.info("fileUrl == >>文件路径为空...");
			return;
		}
		try {
			StorePath storePath = StorePath.parseFromUrl(fileUrl);
			storageClient.deleteFile(storePath.getGroup(), storePath.getPath());
		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
	}
}
