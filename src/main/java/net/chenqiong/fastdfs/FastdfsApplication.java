package net.chenqiong.fastdfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @ClassName: FastdfsApplication
 * @Description: fastdfs测试项目启动类
 * @author: 陈琼
 * @connect：907172474@qq.com
 * @date: 2021年4月27日 下午5:55:08
 */
@SpringBootApplication
@EnableSwagger2
public class FastdfsApplication {
	public static void main(String[] args) {
		SpringApplication.run(FastdfsApplication.class, args);
	}
}
